package app.team;

public class Coordinates {
    private int x, y;

    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Coordinates getShift(int xShift, int yShift) {
        return new Coordinates(getX() + xShift, getY() + yShift);
    }

    @Override
    public int hashCode() {
        return x + 512 * y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Coordinates) {
            Coordinates castedObj = (Coordinates) obj;
            return x == castedObj.x && y == castedObj.y;
        }
        return false;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
