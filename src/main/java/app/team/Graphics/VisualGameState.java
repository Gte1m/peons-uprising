package app.team.Graphics;

import java.util.Set;

public interface VisualGameState {
    Set<VisualPawn> getPawns();
    VisualBoard getBoard();
}
