package app.team.Graphics;

import app.team.Graphics.Menu.SaveGameWindow;
import app.team.Graphics.VisualPawn.Action;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.*;

public class GamePresenter {
    private app.team.Graphics.ConnectionWithGameLogic.GameModel model;
    private GameView view;
    private VisualGameState currentGameState;

    public void setModel(app.team.Graphics.ConnectionWithGameLogic.GameModel model) {
        this.model = model;
    }

    public void setView(app.team.Graphics.GameView view) {
        this.view = view;
    }

    private Button getSaveButton() {
        Button button = new Button();
        double scale = 70;

        button.setLayoutX(2 * scale);
        button.setLayoutY(0);
        button.setPrefWidth(2 * scale);
        button.setPrefHeight(scale / 2);
        button.setText("Save");
        button.setOnAction(event -> new SaveGameWindow(new Stage(), model).run());

        return button;
    }

    private Button getUndoButton() {
        Button button = new Button();
        double scale = 70;

        button.setLayoutX(0);
        button.setLayoutY(0);
        button.setPrefWidth(2 * scale);
        button.setPrefHeight(scale / 2);
        button.setText("Undo");
        button.setOnAction(event -> model.undo());

        return button;
    }

    private HBox getBox() {
        HBox box = new HBox();
        double scale = 70;

        box.setLayoutX(0);
        box.setLayoutY(0);
        box.resize(8 * scale, scale / 2);

        return box;
    }

    private void refreshGameView() {
        Set<VisualPawn> pawns = currentGameState.getPawns();
        Map<VisualPawn, Button> buttonsMap = new HashMap<>();
        Map<VisualPawn, Node> appearanceMap = new HashMap<>();
        Map<VisualPawn, Set<Button>> onClickButtonsMap = new HashMap<>();

        // inserting data to appearanceMap
        for (VisualPawn pawn : pawns) {
            appearanceMap.put(pawn, pawn.getAsShape());
        }

        // inserting data to buttonsMap
        for (VisualPawn pawn : pawns) {
            if (pawn.hasAnyMove()) {
                Action actionOnClick = () -> {
                    for (VisualPawn p : pawns) {
                        if (p.hasAnyMove()) {
                            view.hide(buttonsMap.get(p));
                            view.show(appearanceMap.get(p));
                        }
                    }
                    onClickButtonsMap.get(pawn).forEach(button -> view.show(button));
                };
                buttonsMap.put(pawn, pawn.getAsButton(actionOnClick));
            }
        }

        // inserting data to onClickButtonsMap
        for (VisualPawn pawn : pawns) {
            if (pawn.hasAnyMove()) {
                VisualPawn.ActionFactory actionFactory = coordinates -> () -> {
                    try {
                        MoveOnVisualBoard move = pawn.getMoveOnVisualBoard(coordinates);
                        model.takeSubmit(move);
                    } catch (IllegalMoveOnVisualBoardException exc) {
                        exc.printStackTrace();
                    }
                };
                onClickButtonsMap.put(pawn, pawn.getOnClickButtons(actionFactory));
            }
        }


        HBox box = getBox();
        Button undoButton = getUndoButton();
        Button saveButton = getSaveButton();
        box.getChildren().addAll(undoButton, saveButton);

        view.clear(); // removing elements from old session

        view.add(box);

        // adding board to view
        view.addBoard(currentGameState.getBoard());

        // adding buttons to view
        view.addAll(buttonsMap.values());
        view.showAll(buttonsMap.values());

        // adding appearances to view
        view.addAll(appearanceMap.values());
        for (VisualPawn pawn : appearanceMap.keySet()) {
            Node s = appearanceMap.get(pawn);
            if (buttonsMap.containsKey(pawn)) {
                view.hide(s);
            } else {
                view.show(s);
            }
        }

        // adding onClickButtons to view
        for (Set<Button> buttons : onClickButtonsMap.values()) {
            view.addAll(buttons);
            view.hideAll(buttons);
        }

        // TODO: add button hiding onClickButtons

        view.showStage();
    }

    public void refresh() {
        HBox box = getBox();
        box.getChildren().add(getUndoButton());
        view.add(box);
        view.show(box);

        currentGameState = model.getGameState();
        refreshGameView();
    }

}


/*
 private Button getUndoButton() {
        double scale = 70;
        Button button = new NodeFactory(scale / 2)
                .getSquareButton(new app.team.Coordinates(0, 0), WHITE, () -> model.undo());

        button.getShape().resize(scale / 2, scale * 2);
        button.resize(scale / 2, 3 * scale );
        button.setText("Undo");

        button.setLayoutX(0);
        button.setLayoutY(0);

        return button;
    }
 */
