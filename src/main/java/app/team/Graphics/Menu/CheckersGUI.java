package app.team.Graphics.Menu;

import app.team.GameLogic.Game;
import app.team.GameLogic.GameFactory;
import app.team.Graphics.ConnectionWithGameLogic.GameModelInGUI;
import javafx.stage.Stage;

public class CheckersGUI implements Runnable {
    private static volatile double scale = 70;
    private Stage stage;
    private GameFactory factory;

    public CheckersGUI(Stage stage, GameFactory factory) {
        this.stage = stage;
        this.factory = factory;
    }

    public static void setScale(double scale) {
        CheckersGUI.scale = scale;
    }


    @Override
    public void run() {
        stage.setTitle("Checkers");
        app.team.Graphics.GamePresenter presenter = new app.team.Graphics.GamePresenter();
        app.team.Graphics.GameView view = new app.team.Graphics.GameViewImpl(stage);
        GameModelInGUI model = new GameModelInGUI();
        model.addPresenter(presenter);
        presenter.setModel(model);
        presenter.setView(view);

        Game game = factory.produce(model, scale);
        game.run();
    }
}

