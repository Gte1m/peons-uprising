package app.team.Graphics.Menu;

import app.team.GameLogic.Game;
import app.team.Graphics.VisualPawn.Action;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class StartingMenu extends Application {
    private Button getCasualButton(String text, Action action) {
        double scale = 50;
        Button button = new Button();

        button.setPrefWidth(8 * scale);
        button.setPrefHeight(scale);
        button.setText(text);
        button.setOnAction(event -> action.execute());

        return button;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        Scene scene = new Scene(root);
        Button startNewGameButton = getCasualButton("Start new game",
                () -> new CheckersGUI(new Stage(), Game::getGameFromBeggining).run());
        Button startSavedGameButton = getCasualButton("Start saved game",
                () -> new OpenSavedGameScene(new Stage()).run());

        startNewGameButton.setLayoutX(50);
        startNewGameButton.setLayoutY(50);
        startNewGameButton.setVisible(true);

        startSavedGameButton.setLayoutX(50);
        startSavedGameButton.setLayoutY(150);
        startSavedGameButton.setVisible(true);

        root.getChildren().addAll(startNewGameButton, startSavedGameButton);
        primaryStage.setWidth(500);
        primaryStage.setHeight(300);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Peons Uprising");
        primaryStage.show();
    }

    public static void main(String... args) {
        Application.launch();
    }
}
