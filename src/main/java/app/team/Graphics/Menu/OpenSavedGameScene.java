package app.team.Graphics.Menu;

import app.team.GameLogic.Game;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OpenSavedGameScene implements Runnable {
    private Stage stage;

    public OpenSavedGameScene(Stage stage) {
        this.stage = stage;
    }

    private List<String> getSaveNames() {
        Pattern pattern = Pattern.compile(".*\\.txt");
        List<String> results = List.of();

        try (Stream<Path> paths = Files.walk(Paths.get("resources/saves"))) {
            results = paths
                    .filter(Files::isRegularFile)
                    .map(Object::toString)
                    .filter(name -> pattern.matcher(name).matches())
                    .map(name -> {
                        int len = name.length();
                        return name.substring(16, len - 4);
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return results;
    }

    private ComboBox<String> getComboBoxWithSaveNames() {
        ComboBox<String> comboBox = new ComboBox<>();
        List<String> saveNames = getSaveNames();
        double scale = 50;

        comboBox.getItems().addAll(saveNames);
        comboBox.setVisibleRowCount(5);
        comboBox.setPrefWidth(scale * 8);
        comboBox.setPrefHeight(scale);

        return comboBox;
    }

    private Button getOKButton(Supplier<String> saveNameSupplier) {
        Button button = new javafx.scene.control.Button();
        double scale = 50;

        button.setText("OK");
        button.setPrefWidth(scale * 3);
        button.setPrefHeight(scale);
        button.setOnAction(event -> {
            String saveName = saveNameSupplier.get();
            CheckersGUI checkers = new CheckersGUI(stage, (model, s) -> Game.getGameFromSave(model, s, saveName));
            checkers.run();
        });

        return button;
    }

    private Button getDeleteSaveButton(ComboBox<String> comboBox) {
        Button button = new javafx.scene.control.Button();
        double scale = 50;

        button.setText("Delete");
        button.setPrefWidth(scale * 3);
        button.setPrefHeight(scale);
        button.setOnAction(event -> {
            String saveName = comboBox.getValue();
            comboBox.getItems().remove(saveName);
            Game.deleteSave(saveName);
        });

        return button;
    }

    public void run() {
        Group root = new Group();
        Scene scene = new Scene(root);
        ComboBox<String> box = getComboBoxWithSaveNames();
        Button okButton = getOKButton(box::getValue);
        Button deleteSaveButton = getDeleteSaveButton(box);

        box.setLayoutX(50);
        box.setLayoutY(50);
        deleteSaveButton.setLayoutX(300);
        deleteSaveButton.setLayoutY(150);
        okButton.setLayoutX(50);
        okButton.setLayoutY(150);
        root.getChildren().addAll(box, okButton, deleteSaveButton);

        stage.setWidth(500);
        stage.setHeight(300);
        stage.setScene(scene);
        stage.setTitle("Open");
        stage.show();
    }
}
