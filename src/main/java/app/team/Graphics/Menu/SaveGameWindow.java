package app.team.Graphics.Menu;

import app.team.Graphics.ConnectionWithGameLogic.GameModel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Supplier;

public class SaveGameWindow implements Runnable {
    private Stage stage;
    private GameModel model;

    public SaveGameWindow(javafx.stage.Stage stage, GameModel model) {
        this.stage = stage;
        this.model = model;
    }

    private TextField getTextField() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss");
        Date currentTime = new Date(System.currentTimeMillis());
        String initialText = dateFormat.format(currentTime);

        TextField textField = new TextField();
        textField.setText(initialText);
        textField.setPrefWidth(300);
        textField.setPrefHeight(30);

        return textField;
    }

    private Button getOKButton(Supplier<String> saveNameSupplier) {
        Button button = new Button();

        button.setPrefWidth(150);
        button.setPrefHeight(40);
        button.setText("OK");
        button.setOnAction(event -> {
            String saveName = saveNameSupplier.get();
            if (saveName != null) {
                model.save(saveName);
                stage.close();
            }
        });

        return button;
    }

    public void run() {
        Group root = new Group();
        Scene scene = new Scene(root);

        TextField textField = getTextField();
        Button okButton = getOKButton(textField::getText);

        textField.setLayoutX(50);
        textField.setLayoutY(20);
        okButton.setLayoutX(125);
        okButton.setLayoutY(80);

        root.getChildren().addAll(textField, okButton);
        stage.setWidth(400);
        stage.setHeight(180);
        stage.setScene(scene);
        stage.setTitle("Save");
        stage.show();
    }



}
