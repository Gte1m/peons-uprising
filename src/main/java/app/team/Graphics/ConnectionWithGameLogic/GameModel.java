package app.team.Graphics.ConnectionWithGameLogic;

public interface GameModel {
    app.team.Graphics.VisualGameState getGameState();
    boolean takeSubmit(app.team.Graphics.MoveOnVisualBoard move);
    void addPresenter(app.team.Graphics.GamePresenter presenter);
    void undo();
    void save(String saveName);
    void deleteSave(String saveName);
}
