package app.team.Graphics.ConnectionWithGameLogic;

import app.team.Graphics.VisualGameState;

public interface GUIInterface {
    void actualize(VisualGameState gameState);
    app.team.Graphics.MoveOnVisualBoard getNextMove();
    app.team.Graphics.MoveOnVisualBoard popNextMove();
    boolean hasPendingMoves();
    void setGame(app.team.GameLogic.Game game);
}
