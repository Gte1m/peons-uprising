package app.team.Graphics.ConnectionWithGameLogic;

public class GameModelInGUISingleton {
    private static class Helper {
        static GameModelInGUI model = new GameModelInGUI();
        static GameModelInGUI getModel() {
            return model;
        }
    }

    public static GameModelInGUI getGameModelImpl() {
        return Helper.getModel();
    }
}
