package app.team.Graphics.ConnectionWithGameLogic;

import app.team.GameLogic.Game;
import app.team.Graphics.GamePresenter;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class GameModelInGUI implements GameModel, GUIInterface {
    private Queue<app.team.Graphics.MoveOnVisualBoard> movesQueue = new ConcurrentLinkedQueue<>();
    private volatile app.team.Graphics.VisualGameState nextGameState;
    private Collection<GamePresenter> presenters = new ConcurrentLinkedQueue<>();
    private Game game;

    @Override
    public app.team.Graphics.VisualGameState getGameState() {
        return nextGameState;
    }

    @Override
    public void actualize(app.team.Graphics.VisualGameState gameState) {
        this.nextGameState = gameState;
        presenters.forEach(GamePresenter::refresh);
    }

    @Override
    public app.team.Graphics.MoveOnVisualBoard getNextMove() {
        return movesQueue.element();
    }

    @Override
    public app.team.Graphics.MoveOnVisualBoard popNextMove() {
        return movesQueue.remove();
    }

    @Override
    public boolean hasPendingMoves() {
        return !movesQueue.isEmpty();
    }

    @Override
    public void setGame(app.team.GameLogic.Game game) {
        this.game = game;
    }

    @Override
    public boolean takeSubmit(app.team.Graphics.MoveOnVisualBoard move) {
        boolean result = movesQueue.add(move);
        game.refresh();
        return result;
    }

    @Override
    public void addPresenter(GamePresenter presenter) {
        presenters.add(presenter);
    }

    @Override
    public void undo() {
        game.undo();
    }

    @Override
    public void save(String saveName) {
        game.save(saveName);
    }

    @Override
    public void deleteSave(String saveName) {
        game.deleteSave(saveName);
    }

}
