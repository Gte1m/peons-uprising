package app.team.Graphics;

import javafx.scene.Node;
import javafx.scene.shape.Shape;

import java.util.Collection;
import java.util.Set;

public interface GameView {
    void add(Node element);
    void remove(Node element);
    boolean contains(Node element);
    void clear();
    void showStage();
    void hideStage();

    default void addBoard(VisualBoard board) {
        Set<Shape> shapes = board.getFieldsAsShapes();
        addAll(shapes);
        showAll(shapes);
    }

    default void addAll(Collection<? extends Node> elements) {
        for (Node e : elements) {
            add(e);
        }
    }

    default void addAll(Node... elements) {
        for (Node e : elements) {
            add(e);
        }
    }

    default void show(Node element) {
        element.setVisible(true);
        element.toFront();
    }

    default void showAll(Collection<? extends Node> elements) {
        for (Node n : elements) {
            show(n);
        }
    }

    default void hide(Node element) {
        element.setVisible(false);
    }

    default void hideAll(Collection<? extends Node> elements) {
        for (Node n : elements) {
            hide(n);
        }
    }
}
