package app.team.Graphics;

public interface MoveOnVisualBoard {
    app.team.Coordinates getFrom();
    app.team.Coordinates getTo();
}
