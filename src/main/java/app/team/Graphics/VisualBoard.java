package app.team.Graphics;

import javafx.scene.shape.Shape;

import java.util.Set;

public interface VisualBoard {
    Set<Shape> getFieldsAsShapes();
}
