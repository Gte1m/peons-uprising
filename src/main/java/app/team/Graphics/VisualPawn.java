package app.team.Graphics;

import javafx.scene.control.*;
import javafx.scene.shape.*;

import java.util.*;


public interface VisualPawn {

    @FunctionalInterface
    interface Action {
        void execute();
    }

    @FunctionalInterface
    interface ActionFactory {
        Action produce(app.team.Coordinates coordinates);
    }

    Button getAsButton(Action action);
    Shape getAsShape();
    Set<Button> getOnClickButtons(ActionFactory factory);
    Set<app.team.Coordinates> getPossibleMoves();

    boolean hasAnyMove();

    MoveOnVisualBoard getMoveOnVisualBoard(app.team.Coordinates dest)
            throws IllegalMoveOnVisualBoardException;
}
