package app.team.Graphics;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;

import java.util.*;

public class GameViewImpl implements GameView {
    private Set<Node> currentSession = new HashSet<>();
    private Group root = new Group();
    private Scene scene = new Scene(root);
    private Stage stage;

  //  {
      //  scene.setFill(Color.AQUA);
  //  }

    public GameViewImpl(Stage stage) {
        this.stage = stage;
        stage.setScene(scene);
    }

    @Override
    public void showStage() {
        stage.sizeToScene();
        stage.show();
    }

    @Override
    public void hideStage() {
        stage.hide();
    }

    @Override
    public void add(Node element) {
        root.getChildren().add(element);
        currentSession.add(element);
    }

    @Override
    public void addAll(Collection<? extends Node> elements) {
        elements.forEach(this::add);
    }

    @Override
    public void remove(Node element) {
        root.getChildren().remove(element);
        currentSession.remove(element);
    }

    public void removeAll(Collection<? extends Node> elements) {
        elements.forEach(this::remove);
    }

    @Override
    public boolean contains(Node element) {
        return root.getChildren().contains(element);
    }

    @Override
    public void clear() {
        root.getChildren().removeAll(currentSession);
        currentSession.clear();
    }
}

