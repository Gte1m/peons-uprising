package app.team.Graphics.CheckersGraphics;

import app.team.Coordinates;
import app.team.Graphics.NodeFactory;
import app.team.Graphics.VisualPawn.Action;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

public class CheckersNodeQueenFactory implements CheckersNodeFactory {
    private NodeFactory factory;

    public CheckersNodeQueenFactory(double scale) {
        this.factory = new NodeFactory(scale);
    }

    @Override
    public Shape getShape(Coordinates coordinates, Color color) {
        return factory.getOctagon(coordinates, color);
    }


    @Override
    public Button getButton(Coordinates coordinates, Color color, Action action) {
        return factory.getOctagonButton(coordinates, color, action);
    }
}
