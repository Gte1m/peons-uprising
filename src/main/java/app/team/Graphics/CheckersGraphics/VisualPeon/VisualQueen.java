package app.team.Graphics.CheckersGraphics.VisualPeon;

import app.team.Graphics.CheckersGraphics.CheckerVisualColor;
import app.team.Graphics.CheckersGraphics.CheckersNodeFactory;
import app.team.Graphics.CheckersGraphics.CheckersNodeQueenFactory;
import app.team.Coordinates;

import java.util.Set;

public class VisualQueen extends AbstractVisualPeon {
    private CheckersNodeFactory queenFactory;

    VisualQueen(Coordinates coordinates, Set<Coordinates> possibleMoves,
               CheckerVisualColor color, double scale) {
        super(coordinates, possibleMoves, color, scale);
        this.queenFactory = new CheckersNodeQueenFactory(scale);
    }

    @Override
    CheckersNodeFactory getVisualPeonFactory() {
        return queenFactory;
    }


}
