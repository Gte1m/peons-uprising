package app.team.Graphics.CheckersGraphics.VisualPeon;

import app.team.Graphics.CheckersGraphics.CheckerVisualColor;
import app.team.Coordinates;

import java.util.Set;

public class VisualQueenFactory implements app.team.Graphics.CheckersGraphics.VisualPeon.CheckersPawnFactory {
    private double scale;

    public VisualQueenFactory(double scale) {
        this.scale = scale;
    }

    public VisualQueen getWhite(Coordinates coordinates, Set<Coordinates> possibleMoves) {
        return new VisualQueen(coordinates, possibleMoves, CheckerVisualColor.WHITE, scale);
    }

    public VisualQueen getBlack(Coordinates coordinates, Set<Coordinates> possibleMoves) {
        return new VisualQueen(coordinates, possibleMoves, CheckerVisualColor.BLACK, scale);
    }

    public VisualQueen getPawn(Coordinates coordinates, Set<Coordinates> possibleMoves,
                                      CheckerVisualColor color) {
        return new VisualQueen(coordinates, possibleMoves, color, scale);
    }
}
