package app.team.Graphics.CheckersGraphics.VisualPeon;

import app.team.Graphics.CheckersGraphics.CheckerVisualColor;
import app.team.Graphics.CheckersGraphics.CheckersNodeFactory;
import app.team.Coordinates;
import app.team.Graphics.IllegalMoveOnVisualBoardException;
import app.team.Graphics.MoveOnVisualBoard;
import app.team.Graphics.MoveOnVisualBoardImpl;
import app.team.Graphics.NodeFactory;
import app.team.Graphics.VisualPawn;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractVisualPeon implements VisualPawn {
    protected Coordinates currentCoordinates;
    private Set<Coordinates> possibleMoves;
    protected CheckerVisualColor color;
    private double scale;

    AbstractVisualPeon(Coordinates coordinates, Set<Coordinates> possibleMoves,
                       CheckerVisualColor color, double scale) {
        this.currentCoordinates = coordinates;
        this.possibleMoves = possibleMoves;
        this.color = color;
        this.scale = scale;
    }

    abstract CheckersNodeFactory getVisualPeonFactory();

    @Override
    public Button getAsButton(VisualPawn.Action action) {
        return getVisualPeonFactory().getButton(currentCoordinates, color.getColor(), action);
    }

    @Override
    public Shape getAsShape() {
        return getVisualPeonFactory().getShape(currentCoordinates, color.getColor());
    }

    @Override
    public Set<Button> getOnClickButtons(ActionFactory actionFactory) {
        NodeFactory nodeFactory = new NodeFactory(scale);
        Set<Button> result = new HashSet<>();

        for (Coordinates coordinates : getPossibleMoves()) {
            Color buttonColor = Color.CORAL; // TODO: next color to think about
            Action actionOnClick = actionFactory.produce(coordinates);
            Button button = nodeFactory.getSquareButton(coordinates,
                    buttonColor, actionOnClick);
            result.add(button);
        }

        return result;
    }

    @Override
    public Set<Coordinates> getPossibleMoves() {
        return possibleMoves;
    }

    @Override
    public boolean hasAnyMove() {
        return !getPossibleMoves().isEmpty();
    }

    @Override
    public MoveOnVisualBoard getMoveOnVisualBoard(Coordinates dest)
            throws IllegalMoveOnVisualBoardException {
        if (!possibleMoves.contains(dest)) {
            throw new IllegalMoveOnVisualBoardException(currentCoordinates, dest);
        }
        return new MoveOnVisualBoardImpl(currentCoordinates, dest);
    }
}
