package app.team.Graphics.CheckersGraphics.VisualPeon;

import app.team.Graphics.CheckersGraphics.CheckerVisualColor;
import app.team.Coordinates;

import java.util.Set;

public class VisualPeonFactory implements CheckersPawnFactory {
    private double scale;

    public VisualPeonFactory(double scale) {
        this.scale = scale;
    }

    public VisualPeon getWhite(Coordinates coordinates, Set<Coordinates> possibleMoves) {
        return new VisualPeon(coordinates, possibleMoves, CheckerVisualColor.WHITE, scale);
    }

    public VisualPeon getBlack(Coordinates coordinates, Set<Coordinates> possibleMoves) {
        return new VisualPeon(coordinates, possibleMoves, CheckerVisualColor.BLACK, scale);
    }

    public VisualPeon getPawn(Coordinates coordinates, Set<Coordinates> possibleMoves,
                              CheckerVisualColor color) {
        return new VisualPeon(coordinates, possibleMoves, color, scale);
    }
}
