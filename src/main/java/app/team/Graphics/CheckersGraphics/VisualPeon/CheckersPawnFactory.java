package app.team.Graphics.CheckersGraphics.VisualPeon;

import app.team.Coordinates;
import app.team.Graphics.VisualPawn;

import java.util.Set;

public interface CheckersPawnFactory {
    VisualPawn getWhite(Coordinates coordinates, Set<Coordinates> possibleMoves);
    VisualPawn getBlack(Coordinates coordinates, Set<Coordinates> possibleMoves);
}
