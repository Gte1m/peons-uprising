package app.team.Graphics.CheckersGraphics.VisualPeon;

import app.team.Graphics.CheckersGraphics.CheckerVisualColor;
import app.team.Graphics.CheckersGraphics.CheckersNodeFactory;
import app.team.Graphics.CheckersGraphics.CheckersNodePeonFactory;
import app.team.Coordinates;

import java.util.Set;

public class VisualPeon extends AbstractVisualPeon {
    /*
    * contains implementation of getVisualPeonFactory() method, which is desired because of
    * extending AbstractVisualPeon class
    */

    private CheckersNodeFactory peonFactory;

    VisualPeon(Coordinates coordinates, Set<Coordinates> possibleMoves,
               CheckerVisualColor color, double scale) {
        super(coordinates, possibleMoves, color, scale);
        this.peonFactory = new CheckersNodePeonFactory(scale);
    }

    @Override
    CheckersNodeFactory getVisualPeonFactory() {
        return peonFactory;
    }
}
