package app.team.Graphics.CheckersGraphics;

import app.team.Coordinates;
import app.team.Graphics.NodeFactory;
import app.team.Graphics.VisualBoard;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

import java.util.*;

public class VisualCheckersBoard implements VisualBoard {
    private double scale;

    public VisualCheckersBoard(double scale) {
        this.scale = scale;
    }

    @Override
    public Set<Shape> getFieldsAsShapes() {
        NodeFactory nodeFactory = new NodeFactory(scale);
        Set<Shape> result = new HashSet<>();

        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                Coordinates coordinates = new Coordinates(i, j);
                Color color = BoardFieldColor.getColorOfField(coordinates).getColor();
                Shape shape = nodeFactory.getSquare(coordinates, color);
                result.add(shape);
            }
        }

        return result;
    }
}
