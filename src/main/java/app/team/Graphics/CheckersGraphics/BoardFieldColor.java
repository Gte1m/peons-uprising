package app.team.Graphics.CheckersGraphics;

import app.team.Coordinates;
import javafx.scene.paint.Color;

public enum BoardFieldColor {
    // TODO: maybe change these colors, idk now

    BLACK {
        @Override
        public Color getColor() {
            return Color.CRIMSON;
        }
    }, WHITE {
        @Override
        public Color getColor() {
            return Color.WHEAT;
        }
    };

    public abstract Color getColor();

    public static BoardFieldColor getColorOfField(Coordinates coordinates) {
        if (coordinates.getX() % 2 == coordinates.getY() % 2) {
            return WHITE;
        }
        return BLACK;
    }

    public static BoardFieldColor getColorOfField(int x, int y) {
        return getColorOfField(new Coordinates(x, y));
    }
}
