package app.team.Graphics.CheckersGraphics;

import app.team.Graphics.VisualBoard;
import app.team.Graphics.VisualGameState;
import app.team.Graphics.VisualPawn;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SimpleVisualGameState implements VisualGameState{
    private VisualBoard board;
    private Set<VisualPawn> pawns = new HashSet<>();

    public SimpleVisualGameState(VisualBoard board, Collection<? extends VisualPawn> pawns) {
        this.board = board;
        this.pawns.addAll(pawns);
    }

    @Override
    public Set<VisualPawn> getPawns() {
        return pawns;
    }

    @Override
    public VisualBoard getBoard() {
        return board;
    }
}
