package app.team.Graphics.CheckersGraphics;

import app.team.Coordinates;
import app.team.Graphics.VisualPawn.Action;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

public interface CheckersNodeFactory {
    Shape getShape(Coordinates coordinates, Color color);
    Button getButton(Coordinates coordinates, Color color, Action action);
}
