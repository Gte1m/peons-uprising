package app.team.Graphics.CheckersGraphics;


import app.team.Coordinates;
import app.team.Graphics.IllegalMoveOnVisualBoardException;
import app.team.Graphics.MoveOnVisualBoard;
import app.team.Graphics.MoveOnVisualBoardImpl;
import app.team.Graphics.VisualPawn;
import javafx.scene.control.Button;
import javafx.scene.shape.Shape;

import java.util.Set;

public class VisualCheckerPeon implements VisualPawn {
    private Shape asShape;
    private Button asButton;
    private Set<Coordinates> possibleMoves;
    private Set<Button> onClickButtons;
    private Coordinates currentCoordidantes;


    @Override
    public Button getAsButton(Action action) {
        return asButton;
    }

    @Override
    public Shape getAsShape() {
        return asShape;
    }

    @Override
    public Set<Button> getOnClickButtons(ActionFactory factory) {
        return onClickButtons;
    }

    @Override
    public Set<Coordinates> getPossibleMoves() {
        return possibleMoves;
    }

    @Override
    public boolean hasAnyMove() {
        return !getPossibleMoves().isEmpty();
    }

    @Override
    public MoveOnVisualBoard getMoveOnVisualBoard(Coordinates dest)
            throws IllegalMoveOnVisualBoardException {
        if (!getPossibleMoves().contains(dest)) {
            throw new IllegalMoveOnVisualBoardException(currentCoordidantes, dest);
        }
        return new MoveOnVisualBoardImpl(currentCoordidantes, dest);
    }
}
