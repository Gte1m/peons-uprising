package app.team.Graphics.CheckersGraphics;

import javafx.scene.paint.Color;

public enum CheckerVisualColor {
    WHITE {
        @Override
        public Color getColor() {
            // TODO: maybe change this on sth more cute?
            return Color.WHITE;
        }
    }, BLACK {
        @Override
        public Color getColor() {
            // TODO: maybe this too?
            return javafx.scene.paint.Color.DARKSLATEGREY;
        }
    };

    public abstract Color getColor();
}
