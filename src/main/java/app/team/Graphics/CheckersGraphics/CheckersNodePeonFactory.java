package app.team.Graphics.CheckersGraphics;

import app.team.Coordinates;
import app.team.Graphics.NodeFactory;
import app.team.Graphics.VisualPawn.Action;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

public class CheckersNodePeonFactory implements CheckersNodeFactory {
    private double scale;
    private NodeFactory factory;

    public CheckersNodePeonFactory(double scale) {
        this.scale = scale;
        this.factory = new NodeFactory(scale);
    }

    @Override
    public Shape getShape(Coordinates coordinates, Color color) {
        return factory.getCircle(coordinates, color);
    }

    @Override
    public Button getButton(Coordinates coordinates, Color color, Action action) {
        return factory.getCircleButton(coordinates, color, action);
    }

}
