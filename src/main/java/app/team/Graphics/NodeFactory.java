package app.team.Graphics;

import app.team.Coordinates;
import app.team.Graphics.VisualPawn.Action;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.util.Arrays;

public class NodeFactory {
    private double scale;
    private double epsilon;
    private final int borderWidth = 100;
    private final String darkerRGB = "rgb(95,0,254)";
    private final String lighterRGB = "rgb(21,240,255)";

    public NodeFactory(double scale) {
        this.scale = scale;
        this.epsilon= scale / 15;
    }

    private void addBorder(Button button) {
        Paint color = button.getBackground().getFills().get(0).getFill();
        if (color.equals(javafx.scene.paint.Color.WHITE)) {
            button.setStyle("-fx-border-width: " + borderWidth + ";");
            button.setStyle("-fx-border-color: " + darkerRGB + ";");
        } else {
            button.setStyle("-fx-border-width: " + borderWidth + ";");
            button.setStyle("-fx-border-color: " + lighterRGB + ";");
        }
    }

    public Circle getCircle(Coordinates coordinates, Color color) {
        Circle circle = new Circle();
        double r = scale / 2 - (epsilon / 2);

        circle.setRadius(r);
        circle.setCenterX(scale * coordinates.getX() + (scale / 2));
        circle.setCenterY(scale * coordinates.getY() + (scale / 2)  + scale / 2);
        circle.setFill(color);

        return circle;
    }

    public Button getCircleButton(Coordinates coordinates, Color color, Action action) {
        Button button = new Button();
        Shape shape = getCircle(coordinates, color);

        button.setShape(shape);
        button.setLayoutX(scale * coordinates.getX() + (epsilon / 2));
        button.setLayoutY(scale * coordinates.getY() + (epsilon / 2)  + scale / 2);
        button.setPrefHeight(scale - epsilon);
        button.setPrefWidth(scale - epsilon);
        button.setBackground(new Background(
                new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)
        ));
        button.setOnAction(event -> action.execute());
        button.resize(scale - epsilon, scale - epsilon);
        addBorder(button);

        return button;
    }

    public Rectangle getSquare(Coordinates coordinates, Color color) {
        Rectangle rectangle = new Rectangle();

        rectangle.setHeight(scale);
        rectangle.setWidth(scale);
        rectangle.setX(scale * coordinates.getX());
        rectangle.setY(scale * coordinates.getY() + scale / 2);
        rectangle.setFill(color);

        return rectangle;
    }

    public Button getSquareButton(Coordinates coordinates, Color color, Action action) {
        Button button = new Button();
        Rectangle rec = getSquare(coordinates, color);

        button.setShape(rec);
        button.setLayoutX(scale * coordinates.getX());
        button.setLayoutY(scale * coordinates.getY()  + scale / 2);
        button.setMinHeight(scale);
        button.setMinWidth(scale);
        button.setPrefHeight(scale);
        button.setPrefWidth(scale);
        button.setBackground(new Background(
                new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)
        ));
        button.setOnAction(event -> action.execute());
        button.resize(scale, scale);

        return button;
    }

    public Shape getOctagon(Coordinates coordinates, Color color) {
        double brakeLen = scale / 3;
        double x = coordinates.getX() * scale;
        double y = coordinates.getY() * scale;

        Polygon polygon = new Polygon();
        polygon.getPoints().addAll(
                x + brakeLen, y + epsilon + scale / 2,
                x + scale - brakeLen, y + epsilon + scale / 2,
                x + scale - epsilon, y + brakeLen + scale / 2,
                x + scale - epsilon, y + scale - brakeLen + scale / 2,
                x + scale - brakeLen, y + scale - epsilon + scale / 2,
                x + brakeLen, y + scale - epsilon + scale / 2,
                x + epsilon, y + scale - brakeLen + scale / 2,
                x + epsilon, y + brakeLen + scale / 2);
        polygon.setFill(color);

        return polygon;
    }

    public Button getOctagonButton(Coordinates coordinates, Color color, Action action) {
        Shape shape = getOctagon(coordinates, color);
        Button button = new Button();

        button.setShape(shape);
        button.setLayoutX(scale * coordinates.getX() + epsilon);
        button.setLayoutY(scale * coordinates.getY() + epsilon + scale / 2);
        button.setMinHeight(scale - 2 * epsilon);
        button.setMinWidth(scale - 2 * epsilon);
        button.setPrefHeight(scale - 2 *epsilon);
        button.setPrefWidth(scale - 2 * epsilon);
        button.setBackground(new Background(
                new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)
        ));
        button.setOnAction(event -> action.execute());
        button.resize(scale - 2 * epsilon, scale - 2 * epsilon);
        addBorder(button);

        return button;
    }

    
}
