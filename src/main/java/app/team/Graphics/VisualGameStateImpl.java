package app.team.Graphics;

import java.util.*;

public class VisualGameStateImpl implements VisualGameState {
    private Set<VisualPawn> pawns;
    private VisualBoard board;

    public VisualGameStateImpl(VisualBoard board, Set<VisualPawn> pawns) {
        this.board = board;
        this.pawns = pawns;
    }

    public Set<VisualPawn> getPawns() {
        return pawns;
    }

    public VisualBoard getBoard() {
        return board;
    }
}
