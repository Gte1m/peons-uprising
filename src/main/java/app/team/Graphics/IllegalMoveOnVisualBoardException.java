package app.team.Graphics;

public class IllegalMoveOnVisualBoardException extends Exception {
    private String message;

    public IllegalMoveOnVisualBoardException(app.team.Coordinates from, app.team.Coordinates to) {
        message = "Illegal move from " + from + " to " + to;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

