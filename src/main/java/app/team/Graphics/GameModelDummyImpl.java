package app.team.Graphics;

import app.team.Graphics.CheckersGraphics.VisualCheckersBoard;
import app.team.Graphics.CheckersGraphics.VisualPeon.VisualPeonFactory;

import java.util.HashSet;
import java.util.*;

public class GameModelDummyImpl implements app.team.Graphics.ConnectionWithGameLogic.GameModel {
    private final double scale = 50;
    private Set<VisualPawn> pawns = new HashSet<>();
    private Map<app.team.Coordinates, VisualPawn> map = new java.util.HashMap<>();
    private List<GamePresenter> presenters = new ArrayList<>();

    {
        VisualPeonFactory peonFactory = new VisualPeonFactory(scale);
        for (int i = 0; i < 8; ++i) {
            Set<app.team.Coordinates> set = new HashSet<>();
            set.add(new app.team.Coordinates(i, (i + 1) % 8));

            VisualPawn peon = peonFactory.getBlack(new app.team.Coordinates(i, i), set);
            pawns.add(peon);

            map.put(new app.team.Coordinates(i,i), peon);
        }
    }



    @Override
    public VisualGameState getGameState() {
        VisualBoard board = new VisualCheckersBoard(scale);
        return new VisualGameStateImpl(board, pawns);
    }

    @Override
    public boolean takeSubmit(app.team.Graphics.MoveOnVisualBoard move) {
        if (map.containsKey(move.getTo())) return false;
        VisualPawn p = map.get(move.getFrom());
        map.remove(move.getFrom());
        pawns.remove(p);

        Set<app.team.Coordinates> set = new HashSet<>();
        app.team.Coordinates dest = move.getTo();
        app.team.Coordinates shift = new app.team.Coordinates(dest.getX(), (dest.getY() + 1) % 8);
        VisualPeonFactory fac = new VisualPeonFactory(scale);
        VisualPawn pawn = fac.getBlack(dest, Set.of(shift));
        pawns.add(pawn);
        map.put(move.getTo(), pawn);

        presenters.forEach(GamePresenter::refresh);

        return true;
    }

    @Override
    public void addPresenter(GamePresenter presenter) {
        presenters.add(presenter);
    }

    @Override
    public void undo() {

    }

    @Override
    public void save(String saveName) {

    }

    @Override
    public void deleteSave(String saveName) {

    }


    public void actualize(app.team.Graphics.VisualGameState visualGameState) {
        // pass
    }

    public app.team.Graphics.MoveOnVisualBoard popNextMove() {
        // pass
        return null;
    }
}
