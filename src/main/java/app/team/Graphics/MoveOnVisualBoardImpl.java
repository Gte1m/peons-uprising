package app.team.Graphics;

public class MoveOnVisualBoardImpl implements MoveOnVisualBoard {
    private app.team.Coordinates from, to;

    public MoveOnVisualBoardImpl(app.team.Coordinates from, app.team.Coordinates to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public app.team.Coordinates getFrom() {
        return from;
    }

    @Override
    public app.team.Coordinates getTo() {
        return to;
    }
}
