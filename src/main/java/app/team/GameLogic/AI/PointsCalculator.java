package app.team.GameLogic.AI;

import app.team.Coordinates;
import app.team.GameLogic.CheckersBoard;
import app.team.GameLogic.CheckersColor;
import app.team.GameLogic.Move;
import app.team.GameLogic.Pawn;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class PointsCalculator {
    private int pointsForQueen;
    private int pointsForPeon;
    private Random randomGenerator = new Random();

    public PointsCalculator(int pointsForPeon, int pointsForQueen) {
        this.pointsForPeon = pointsForPeon;
        this.pointsForQueen = pointsForQueen;
    }

    private int valueOfMove(CheckersBoard board, Move move) {
        return -1;
    }

    private int dangersOfBeat(CheckersBoard board, CheckersColor color) {
        Set<Coordinates> inDanger = new HashSet<>();

        for (Pawn pawn : board.getAllPawns()) {
            if (!pawn.getColor().equals(color)) {
                for (Move move : pawn.getPossibleMoves()) {
                    if (move.isBrakingMove()) {
                        inDanger.add(move.getBeatingPlace());
                    }
                }
            }
        }

        return inDanger.size();
    }

    public Move getBestMove(CheckersBoard board) {
        CheckersBoard copyBoard = board.getCopy();
        return null;

    }
}
