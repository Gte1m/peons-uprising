package app.team.GameLogic;

import app.team.Graphics.ConnectionWithGameLogic.GUIInterface;

@FunctionalInterface
public interface GameFactory {
    Game produce(GUIInterface gui, double scale);
}
