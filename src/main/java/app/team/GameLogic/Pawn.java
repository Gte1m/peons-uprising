package app.team.GameLogic;

import app.team.Coordinates;
import app.team.Graphics.VisualPawn;

import java.util.Scanner;
import java.util.Set;

import static app.team.GameLogic.CheckersColor.BLACK;
import static app.team.GameLogic.CheckersColor.WHITE;

public interface Pawn {
    Coordinates getCoordinates();
    CheckersColor getColor();
    Set<Move> getPossibleMoves();
    void upgrade();
    void brake();
    Pawn getCopy();
    void setBoard(CheckersBoard board);
    VisualPawn getAsVisualPawn(double scale);
    VisualPawn getAsPassingVisualPawn(double scale);
    void setCoordinates(Coordinates coordinates);

    default boolean hasAnyMove() {
        return !getPossibleMoves().isEmpty();
    }

    default boolean hasBeatingMove() {
        return getPossibleMoves().stream().anyMatch(Move::isBrakingMove);
    }

    default Move getBrakingMove(Coordinates to, Pawn brakingPawn) {
        return new Move(getCoordinates(), to, brakingPawn.getCoordinates());
    }

    default Move getMove(Coordinates to) {
        return new Move(getCoordinates(), to);
    }

    static Pawn readFromScanner(Scanner scanner) {
        String colorAsString = scanner.next();
        CheckersColor color = colorAsString.equals("WHITE") ? WHITE : BLACK;
        String type = scanner.next();
        Coordinates coordinates = new Coordinates(scanner.nextInt(), scanner.nextInt());

        if (type.equals("PEON")) {
            return new Peon(color, coordinates);
        }
        return new Queen(color, coordinates);
    }
}
