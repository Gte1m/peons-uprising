package app.team.GameLogic;

import app.team.Graphics.ConnectionWithGameLogic.GUIInterface;
import app.team.Graphics.MoveOnVisualBoard;
import app.team.Graphics.VisualGameState;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;
//import javax.xml.bind.*;

import static app.team.GameLogic.CheckersColor.WHITE;

public class Game implements Runnable {
    private LinkedList<CheckersBoard> gameHistory = new LinkedList<>();
    private CheckersColor playersTurn = WHITE;
    private GUIInterface gui;
    private double scale;

    public static Game getGameFromBeggining(GUIInterface gui, double scale) {
        Game game = new Game(gui, scale);
        game.gameHistory.add(CheckersBoard.startingBoard());
        return game;
    }

    public static Game getGameFromSave(GUIInterface gui, double scale, String saveName) {
        Game game = new Game(gui, scale);

        CheckersBoard savedBoard;
        try (Scanner fileScanner = new Scanner(
                new FileInputStream("resources/saves/" + saveName + ".txt"))) {
            savedBoard = CheckersBoard.readFromScanner(fileScanner);
        } catch (java.io.IOException e) {
            e.printStackTrace();
            savedBoard = CheckersBoard.startingBoard();
        }

        game.gameHistory.add(savedBoard);
        return game;
    }

    private Game(GUIInterface gui, double scale) {
        this.gui = gui;
        gui.setGame(this);
        this.scale = scale;
    }

    @Override
    public void run() {
        initializeGui();
    }

    private void initializeGui() {
        CheckersBoard startingBoard = getCurrentBoardState();
        gui.actualize(startingBoard.getAsVisualGameState(scale));
    }

    private void executeMove(Move move) {
        CheckersBoard newBoard = gameHistory.getFirst().getCopy().getBoardAfterMove(move);
        gameHistory.addFirst(newBoard);
    }

    public void undo() {
        if (gameHistory.size() > 1) {
            gameHistory.pollFirst();
        }

        VisualGameState currentGameState = getCurrentBoardState()
                .getAsVisualGameState(scale);
        gui.actualize(currentGameState);
    }

    private CheckersBoard getCurrentBoardState() {
        return gameHistory.getFirst();
    }

    public void refresh() {
        if (gui.hasPendingMoves()) {
            MoveOnVisualBoard move = gui.getNextMove();
            Pawn movingPawn = getCurrentBoardState().getPawn(move.getFrom());
            CheckersColor currentTurn = getCurrentBoardState().getTurn();
            Set<Move> pawnMoves = movingPawn.getPossibleMoves();
            Optional<Move> optionalMove = pawnMoves.stream()
                    .filter(m -> m.getTo().equals(move.getTo()))
                    .findFirst();

            if (!optionalMove.isPresent()) {
                System.out.println("cos sie popsulo");
                return;
            }

            executeMove(optionalMove.get());

            VisualGameState actualizedVisualGameState = getCurrentBoardState().
                    getAsVisualGameState(scale);

            gui.popNextMove();
            gui.actualize(actualizedVisualGameState);
        }
    }

    public void save(String saveName) {
        String filePath = "resources/saves/" + saveName + ".txt";
        File file = new File(filePath);
        if (! file.exists()) {
            try {
                file.createNewFile();
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }

        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(filePath), "utf-8"))) {
            String boardAsString = getCurrentBoardState().toString();
            writer.write(boardAsString);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteSave(String saveName) {
        String filePath = "resources/saves/" + saveName + ".txt";
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }
}
