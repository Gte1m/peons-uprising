package app.team.GameLogic;

import app.team.Coordinates;
import app.team.Graphics.*;
import app.team.Graphics.CheckersGraphics.*;
import app.team.Graphics.VisualGameState;

import java.util.*;

import static app.team.GameLogic.CheckersColor.BLACK;
import static app.team.GameLogic.CheckersColor.WHITE;

public class CheckersBoard {
    private Map<Coordinates, Pawn> pawns = new HashMap<>();
    private Set<Pawn> trashes = new HashSet<>();
    private CheckersColor turn = app.team.GameLogic.CheckersColor.WHITE;
    private app.team.Coordinates beatSthMomentAgo;

    static CheckersBoard readFromScanner(Scanner scanner) {
        CheckersBoard result = new CheckersBoard();

        scanner.next();
        String turn = scanner.next();
        if (turn.equals("WHITE")) {
            result.setTurn(WHITE);
        } else {
            result.setTurn(BLACK);
        }

        scanner.next();
        String isBeating = scanner.next();
        if (isBeating.equals("true")) {
            Pawn pawn = Pawn.readFromScanner(scanner);
            result.beatSthMomentAgo = pawn.getCoordinates();
        } else {
            result.beatSthMomentAgo = null;
        }

        scanner.next();
        int n = scanner.nextInt();
        for (int i = 0; i < n; ++i) {
            Pawn pawn = Pawn.readFromScanner(scanner);
            pawn.setBoard(result);
            result.pawns.put(pawn.getCoordinates(), pawn);
        }

        scanner.next();
        n = scanner.nextInt();
        for (int i = 0; i < n; ++i) {
            Pawn pawn = Pawn.readFromScanner(scanner);
            pawn.setBoard(result);
            result.trashes.add(pawn);
        }

        return result;
    }

    static CheckersBoard startingBoard() {
        CheckersBoard result = new CheckersBoard();

        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                if (x % 2 != y % 2 && y != 3 && y != 4) {
                    CheckersColor color = y < 3 ? BLACK : WHITE;
                    Coordinates coordinates = new Coordinates(x, y);
                    Peon peon = new Peon(color, coordinates);
                    result.addPawn(peon);
                }
            }
        }

        return result;
    }

    private app.team.Coordinates beatSthMomentAgo() {
        return beatSthMomentAgo;
    }

    private void isBeatingSthNow(app.team.Coordinates coordinates) {
        beatSthMomentAgo = coordinates;
    }

    public CheckersBoard getBoardAfterMove(Move move) {
        CheckersBoard result = getCopy();
        result.executeMove(move);
        return result;
    }

    public void setTurn(app.team.GameLogic.CheckersColor turn) {
        this.turn = turn;
    }

    public app.team.GameLogic.CheckersColor getTurn() {
        return turn;
    }

    public java.util.Collection<Pawn> getAllPawns() {
        return pawns.values();
    }

    public CheckersBoard getCopy() {
        CheckersBoard result = new CheckersBoard();

        for (Pawn p : this.pawns.values()) {
            Pawn copy = p.getCopy();
            copy.setBoard(result);
            result.pawns.put(copy.getCoordinates(), copy);
        }
        for (Pawn trash : trashes) {
            Pawn copy = trash.getCopy();
            copy.setBoard(result);
            result.trashes.add(copy);
        }
        result.setTurn(this.getTurn());

        return result;
    }

    private void executeMove(Move move) {
        if (move.isBrakingMove()) {
            Pawn beatingPawn = pawns.remove(move.getBeatingPlace());
            trashes.add(beatingPawn);
            isBeatingSthNow(move.getTo());
        } else {
            setTurn(getTurn().getSecondColor());
            isBeatingSthNow(null);
        }

        Pawn movingPawn = pawns.get(move.getFrom());
        pawns.remove(move.getFrom());
        movingPawn.setCoordinates(move.getTo());
        pawns.put(movingPawn.getCoordinates(), movingPawn);

        if (beatSthMomentAgo() != null) {
            Pawn pawn = getPawn(beatSthMomentAgo());
            if (pawn.getPossibleMoves().stream().noneMatch(Move::isBrakingMove)) {
                isBeatingSthNow(null);
                setTurn(getTurn().getSecondColor());
            }
        }

        upgradePawns();
    }

    public void addPawn(Pawn pawn) {
        pawns.put(pawn.getCoordinates(), pawn);
        pawn.setBoard(this);
    }

    public void brakePawn(Coordinates coordinates) {
        trashes.add(pawns.get(coordinates));
        pawns.remove(coordinates);
    }

    public void brakePawn(Pawn pawn) {
        if (pawns.values().contains(pawn)) {
            trashes.add(pawn);
            pawns.remove(pawn.getCoordinates());
        }
    }

    public int getScore(CheckersColor color) {
        int counter = 0;

        for (Pawn pawn : trashes) {
            if (!pawn.getColor().equals(color)) {
                ++counter;
            }
        }

        return counter;
    }

    public Pawn getPawn(Coordinates coordinates) {
        return pawns.get(coordinates);
    }

    public boolean containsPawn(Coordinates coordinates) {
        return pawns.containsKey(coordinates);
    }

    public void removePawn(Pawn pawn) {
        pawns.remove(pawn.getCoordinates());
    }

    static public boolean pointsOnBoard(Coordinates c) {
        return c.getX() >= 0 && c.getX() < 8 && c.getY() >= 0 && c.getY() < 8;
    }

    VisualGameState getAsVisualGameState(double scale) {
        VisualBoard board = new VisualCheckersBoard(scale);
        Set<VisualPawn> visualPawns = new HashSet<>();

        if (beatSthMomentAgo() != null) {
            // case when player is continuing sequence of beats

            for (Pawn pawn : pawns.values()) {
                VisualPawn vp;
                if (pawn.getCoordinates().equals(beatSthMomentAgo())) {
                    vp = pawn.getAsVisualPawn(scale);
                } else {
                    vp = pawn.getAsPassingVisualPawn(scale);
                }
                visualPawns.add(vp);
            }
        } else if (pawns.values().stream()
                .filter(pawn -> pawn.getColor().equals(getTurn()))
                .anyMatch(Pawn::hasBeatingMove)) {
            // when there are some beating moves available, then remove ability to execute any not-beating move

            for (Pawn pawn : pawns.values()) {
                VisualPawn vp;
                if (pawn.getColor().equals(getTurn()) && pawn.hasBeatingMove()) {
                    vp = pawn.getAsVisualPawn(scale);
                } else {
                    vp = pawn.getAsPassingVisualPawn(scale);
                }
                visualPawns.add(vp);
            }
        } else {
            // case when there is not any beating move available

            for (Pawn pawn : pawns.values()) {
                VisualPawn vp;
                if (pawn.getColor().equals(getTurn())) {
                    vp = pawn.getAsVisualPawn(scale);
                } else {
                    vp = pawn.getAsPassingVisualPawn(scale);
                }
                visualPawns.add(vp);
            }
        }

        return new VisualGameStateImpl(board, visualPawns);
    }

    private void upgradePawns() {
        List<Pawn> pawnsToUpgrade = new ArrayList<>();

        for (Pawn pawn : pawns.values()) {
            if ((pawn.getColor().equals(WHITE) && pawn.getCoordinates().getY() == 0) ||
                    (pawn.getColor().equals(BLACK) && pawn.getCoordinates().getY() == 7)) {
                pawnsToUpgrade.add(pawn);
            }
        }

        pawnsToUpgrade.forEach(Pawn::upgrade);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("turn ").append(getTurn()).append("\n");
        sb.append("continuing_sequence_of_beats ").append(beatSthMomentAgo() != null)
                .append("\n");
        if (beatSthMomentAgo() != null) {
            sb.append("    ").append(pawns.get(beatSthMomentAgo())).append("\n");
        }

        sb.append("board ").append(pawns.size()).append("\n");
        for (Pawn pawn : pawns.values()) {
            sb.append("    ").append(pawn).append("\n");
        }

        sb.append("trashes ").append(trashes.size()).append("\n");
        for (Pawn pawn : trashes) {
            sb.append("    ").append(pawn).append("\n");
        }

        return sb.toString();
    }
}
