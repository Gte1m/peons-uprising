package app.team.GameLogic;

import app.team.Graphics.CheckersGraphics.CheckerVisualColor;

public enum CheckersColor {
    WHITE {
        @Override
        CheckerVisualColor getVisualColor() {
            return CheckerVisualColor.WHITE;
        }

        @Override
        app.team.GameLogic.CheckersColor getSecondColor() {
            return BLACK;
        }
    }, BLACK {
        @Override
        CheckerVisualColor getVisualColor() {
            return CheckerVisualColor.BLACK;
        }

        @Override
        app.team.GameLogic.CheckersColor getSecondColor() {
            return WHITE;
        }
    };


    abstract CheckerVisualColor getVisualColor();
    abstract CheckersColor getSecondColor();
}
