package app.team.GameLogic;

import app.team.Coordinates;
import app.team.Graphics.CheckersGraphics.VisualPeon.VisualQueenFactory;
import app.team.Graphics.VisualPawn;

import java.util.*;

public class Queen extends Peon {

    public Queen(CheckersColor color, Coordinates coordinates) {
        super(color, coordinates);
    }

    private boolean hasAnyBeatAfterMove(Move move) {
        Coordinates moveDest = move.getTo();
        Coordinates shift = new Coordinates(moveDest.getX() - getCoordinates().getX(),
                moveDest.getY() - getCoordinates().getY());
        Coordinates moveDirection = new Coordinates(shift.getX() > 0 ? 1 : -1,
                shift.getY() > 0 ? 1 : -1);

        for (Coordinates nextMoveDirection : Arrays.asList(
                moveDirection,
                new Coordinates(- moveDirection.getY(), moveDirection.getX()),
                new Coordinates(moveDirection.getY(), - moveDirection.getY())
        )) {
            boolean opponentPawnFound = false;
            Coordinates coor = moveDest.getShift(nextMoveDirection.getX(), nextMoveDirection.getY());

            while (CheckersBoard.pointsOnBoard(coor)) {
                if (getBoard().containsPawn(coor)) {
                    if (opponentPawnFound) {
                        // on previous position we have found opponent pawn, but we cannot beat it,
                        // because of lack of empty field behind it
                        break;
                    } else if (getBoard().getPawn(coor).getColor().equals(getColor())) {
                        break;
                    } else {
                        opponentPawnFound = true;
                    }
                } else if (opponentPawnFound) {
                    // case when we found empty field behind opponent pawn
                    return true;
                }

                coor = coor.getShift(nextMoveDirection.getX(), nextMoveDirection.getY());
            }
        }

        return false;
    }

    @Override
    public java.util.Set<Move> getPossibleMoves() {
        Set<app.team.GameLogic.Move> result = new HashSet<>();

        for (int xShift : Arrays.asList(-1, 1)) {
            for (int yShift : Arrays.asList(-1, 1)) {
                Coordinates dest = getCoordinates().getShift(xShift, yShift);
                Pawn brokenPawn = null;
                boolean brokenPawnFound = false; // true if broken Pawn is not null

                for (; CheckersBoard.pointsOnBoard(dest); dest = dest.getShift(xShift, yShift)) {
                    if (getBoard().containsPawn(dest)) {
                        // case when dest points on field with pawn on it
                        Pawn currPawn = getBoard().getPawn(dest);
                        if (currPawn.getColor().equals(getColor()) || brokenPawnFound) {
                            break;
                        } else {
                            brokenPawnFound = true;
                            brokenPawn = currPawn;
                        }
                    } else {
                        // case when curr points on field without pawn
                        if (brokenPawnFound) {
                            Move move = getBrakingMove(dest, brokenPawn);
                            result.add(move);
                        } else {
                            Move move = getMove(dest);
                            result.add(move);
                        }
                    }
                }
            }
        }

        if (result.stream().anyMatch(Move::isBrakingMove)) {
            result.removeIf(move -> !move.isBrakingMove());
            if (result.stream().anyMatch(this::hasAnyBeatAfterMove)) {
                result.removeIf(move -> !this.hasAnyBeatAfterMove(move));
            }
        }

        return result;
    }

    @Override
    public void upgrade() {
        // pass
    }

    @Override
    public Queen getCopy() {
        Queen copy = new Queen(getColor(), getCoordinates());
        copy.setBoard(getBoard());
        return copy;
    }

    @Override
    public VisualPawn getAsVisualPawn(double scale) {
        VisualQueenFactory factory = new VisualQueenFactory(scale);
        Set<Coordinates> destinations = getPossibleMoves().stream()
                .map(Move::getTo)
                .collect(java.util.stream.Collectors.toSet());

        return factory.getPawn(
                getCoordinates(),
                destinations,
                getColor().getVisualColor()
        );
    }

    @Override
    public VisualPawn getAsPassingVisualPawn(double scale) {
        VisualQueenFactory factory = new VisualQueenFactory(scale);
        return factory.getPawn(
                getCoordinates(),
                Set.of(),
                getColor().getVisualColor()
        );
    }

    @Override
    public String toString() {
        return getColor() + " QUEEN " +
                getCoordinates().getX() + " " + getCoordinates().getY();
    }
}
