package app.team.GameLogic;

import app.team.Coordinates;

public class Move {
    private Coordinates from, to;
    private Coordinates beatingPlace;
    private boolean isBrakingMove;

    public Move(Coordinates from, Coordinates to) {
        this.from = from;
        this.to = to;
        beatingPlace = null;
        isBrakingMove = false;
    }

    public Move(Coordinates from, Coordinates to, app.team.Coordinates beatingPlace) {
        this.from = from;
        this.to = to;
        this.beatingPlace = beatingPlace;
        this.isBrakingMove = true;
    }

    public boolean isBrakingMove() {
        return isBrakingMove;
    }

    public app.team.Coordinates getBeatingPlace() {
        return beatingPlace;
    }

    public Coordinates getFrom() {
        return from;
    }

    public Coordinates getTo() {
        return to;
    }

    @Override
    public int hashCode() {
        return from.hashCode() + to.hashCode() +
                (isBrakingMove ? beatingPlace.hashCode() : 0);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Move) {
            Move move = (Move) obj;
            return move.getFrom().equals(getFrom()) &&
                    move.getTo().equals(getTo()) &&
                    move.isBrakingMove() == isBrakingMove() &&
                    (!isBrakingMove() || move.getBeatingPlace().equals(getBeatingPlace()));
        }
        return false;
    }
}
