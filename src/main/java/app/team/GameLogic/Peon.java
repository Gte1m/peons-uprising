package app.team.GameLogic;

import app.team.Coordinates;
import app.team.Graphics.CheckersGraphics.VisualPeon.VisualPeonFactory;
import app.team.Graphics.VisualPawn;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static app.team.GameLogic.CheckersColor.WHITE;

public class Peon implements Pawn {
    private CheckersColor color;
    private Coordinates coordinates;
    private CheckersBoard board;

    public Peon(CheckersColor color, Coordinates coordinates) {
        this.color = color;
        this.coordinates = coordinates;
    }

    public void setBoard(app.team.GameLogic.CheckersBoard board) {
        this.board = board;
    }

    @Override
    public VisualPawn getAsVisualPawn(double scale) {
        VisualPeonFactory factory = new VisualPeonFactory(scale);
        Set<Coordinates> destinations = getPossibleMoves().stream()
                .map(Move::getTo)
                .collect(Collectors.toSet());

        return factory.getPawn(
                getCoordinates(),
                destinations,
                getColor().getVisualColor()
        );
    }

    @Override
    public VisualPawn getAsPassingVisualPawn(double scale) {
        VisualPeonFactory factory = new VisualPeonFactory(scale);
        return factory.getPawn(
                getCoordinates(),
                Set.of(),
                getColor().getVisualColor()
        );
    }


    protected app.team.GameLogic.CheckersBoard getBoard() {
        return board;
    }

    @Override
    public Coordinates getCoordinates() {
        return coordinates;
    }

    public CheckersColor getColor() {
        return color;
    }

    @Override
    public Set<Move> getPossibleMoves() {
        Set<Move> result = new HashSet<>();
        int direction = getColor().equals(WHITE) ? -1 : 1;

        for (int xShift : Arrays.asList(-1, 1)) {
            for (int yShift : Arrays.asList(-1, 1)) {
                Coordinates dest = getCoordinates().getShift(xShift, yShift);

                // move without braking anything
                if (yShift == direction && CheckersBoard.pointsOnBoard(dest)
                        && !board.containsPawn(dest)) {
                    Move move = getMove(dest);
                    result.add(move);
                }

                // move with braking sth

                Coordinates secondDest = dest.getShift(xShift, yShift);
                if (CheckersBoard.pointsOnBoard(dest)
                        && board.containsPawn(dest) &&
                        ! board.getPawn(dest).getColor().equals(this.getColor()) &&
                        CheckersBoard.pointsOnBoard(secondDest) && !board.containsPawn(secondDest)) {
                    Pawn brakingPawn = board.getPawn(dest);
                    Move move = getBrakingMove(secondDest, brakingPawn);

                    result.add(move);
                }
            }
        }

        if (result.stream().anyMatch(Move::isBrakingMove)) {
            result.removeIf(move -> !move.isBrakingMove());
        }

        return result;
    }

    @Override
    public void upgrade() {
        Queen queen = new Queen(getColor(), getCoordinates());
        board.removePawn(this);
        board.addPawn(queen);
    }

    @Override
    public void brake() {
        board.brakePawn(coordinates);
    }

    @Override
    public Pawn getCopy() {
        Peon copy = new Peon(getColor(), getCoordinates());
        copy.setBoard(board);
        return copy;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Peon) {
            Peon p = (Peon) o;
            return p.getCoordinates().equals(getCoordinates()) && p.getColor().equals(getColor());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getCoordinates().hashCode() + getColor().hashCode() + 123;
    }

    @Override
    public void setCoordinates(app.team.Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return getColor() + " PEON " +
                getCoordinates().getX() + " " + getCoordinates().getY();
    }
}
